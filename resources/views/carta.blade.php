@extends('layouts.app')

@section('content')
    
    <!-- Filter menu -->
      <div class="pm-isotope-filter-container">
        <ul class="pm-isotope-filter-system">
            <li class="pm-isotope-filter-system-expand">Menú <i class="fa fa-angle-down"></i></li>
            <li><a href="#" class="current">La Carta</a></li>
        </ul>
      </div>
    <!-- Filter menu end -->
    <div class="container pm-containerPadding80">
      <div class="row">
      		@foreach($productos as $prod)
	          <div class="col-lg-6 col-md-6 col-sm-6 pm-column-spacing">
	            <div class="pm-store-item-container">
	              <div class="pm-store-item-img-container" style="background-image: url('/productos/{{$prod->tx_img}}')"></div>
	              <div class="pm-store-item-desc">
	                <p class="pm-store-item-title">{{$prod->no_producto}}</p>
	                <div class="pm-store-item-divider"></div>
	                <div class="col-xs-12 col-lg-12" style="margin-top: 28px;">
	                	{!!$prod->des_producto!!}
	                </div>
	              </div>
	            </div>
	          </div>
	        @endforeach
      </div>
    </div>
    
@endsection