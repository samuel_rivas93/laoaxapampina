@extends('layouts.app')

@section('content')

<div class="container pm-containerPadding-top-60 pm-containerPadding-bottom-60">
        	   <div class="row">
                    <div class="col-lg-12">
                
                        <div style="width: 100%"><iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q=Lima,%20Per%C3%BA+(La%20Oxapampina)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe></div>
                    
                    
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container pm-containerPadding-bottom-60">
        	<div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 pm-column-spacing">
                
                      <h6>DIRECCIÓN</h6>
                      <p>Lima - Perú<br></p>
                      
                      <div class="pm-divider" style="margin:20px 0;"></div>
                      
                      <h6>TELÉFONOS</h6>
                      <p><strong>Oficina:</strong>+51915001658<br />
                      
                      <div class="pm-divider" style="margin:20px 0;"></div>
                      
                      <h6>E-mail</h6>
                      <p><a href="mailto:>laoxapampinaperu@gmail.com"><strong>laoxapampinaperu@gmail.com</strong></a><br>
                      </p>
                      
                	<div class="pm-divider" style="margin:20px 0;"></div>
                      
                    <!--<h6>Let's stay in touch</h6>
                    <p><img src="img/qr.png" width="244" height="244"></p> -->
                      
                      
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 pm-column-spacing">
                	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
						  	@if(\Illuminate\Support\Facades\Session::has('envoit-mensaje'))
						        <div class="alert alert-success alert-dismissible">
						            <button type="button" class="close" data-dismiss="alert">&times;</button>{{\Illuminate\Support\Facades\Session::get('envoit-mensaje')}}
						        </div>
						 	@endif
					</div>
                      
                    <h6 class="pm-primary">CONSULTAS DUDAS Y SUGERENCIAS</h6>
                    <div class="pm-contact-form-container">
                    	<p class="pm-required">Envíenos sus consultas y sugerencias mediante nuestro formulario de contacto. Complete todos los campos obligatorio y verifique que la información ingresada es correcta. Gracias!</p><br>
                    	<form id="pm-contact-form" action="#" method="post">
                    		{{csrf_field()}}

                            Seleccione una opción:<br>
                            <input name="pm_s_full_name" id="pm_s_full_name" type="text" placeholder="Nombre *" class="pm-form-textfield">
                            <input name="pm_s_email_address" id="pm_s_email_address" type="text" placeholder="Email *" class="pm-form-textfield">
                           <input name="pm_s_subject" id="pm_s_subject" type="text" placeholder="Teléfono *" class="pm-form-textfield">

							<!--<input name="pm_s_subject" id="pm_s_subject" type="text" placeholder="Asunto *" class="pm-form-textfield">-->
                            <textarea name="pm_s_message" id="pm_s_message" cols="20" rows="6" placeholder="Mensaje *" class="pm-form-textarea"></textarea>
                            <div id="pm-contact-form-response"></div>
                            <input name="pm-form-submit-btn" class="pm-rounded-submit-btn pm-primary" type="button" value="Enviar" id="pm-contact-form-btn" />
                            <input type="hidden" name="pm_s_email_address_contact" value="contacto@dcnaveda.com, form.website.contact@gmail.com" />
                      </form>
                  </div>
                      
                </div>
            </div>
        </div>
        
@endsection

