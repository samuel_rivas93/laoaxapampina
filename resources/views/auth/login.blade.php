@extends('layouts.app')

@section('content')
<div id="tg-homebanner" class="tg-homebanner tg-haslayout">
    <figure class="item" data-vide-bg="poster: images/slider/img-01.jpg" data-vide-options="position: 50% 50%">
        <div style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 50% 50%;">
        </div>
                <figcaption style="padding: 100px 0 21px !important;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="tg-bannercontent">
                                    <div class="container">
                                        <div class="tg-logingarea" style="border-radius: 23px;">
                                            <h2 style="color: black;padding: 22px;border-radius: 23px;">{{ __('Ingresar') }}</h2>
                                            <form method="POST" class="tg-formtheme tg-formloging" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                                @csrf

                                                <fieldset>
                                                    <div class="form-group">
                                                        <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña">
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <button class="tg-btn" type="submit">
                                                        {{ __('Login') }}
                                                    </button>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </figcaption>
    </figure>
</div>
@endsection
