@extends('layouts.app')

@section('content')
    
    <router-view name="detall_product" :productos="{{ json_encode($productos)}}" :nombre_categoria="{{ json_encode($nombre_categoria)}}" :count="{{ json_encode($count)}}" :token_session="{{ json_encode($token_session)}}"></router-view>
    
@endsection