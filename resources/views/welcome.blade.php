@extends('layouts.app')

@section('content')
    
    <router-view name="welcome" :count="{{ json_encode($count)}}" :token_session="{{ json_encode($token_session)}}"></router-view>
    
@endsection