require('./bootstrap');


window.Vue = require('vue');
import VueRouter from 'vue-router';



window.Vue.use(VueRouter);

import Welc from './components/welcome.vue';
import Prod from './components/productos.vue';
import DetPro from './components/detall_product.vue';
import Shop from './components/Custom/Shopping.vue';
import Ped from './components/pedidos.vue';

const routes = [
	{
            path: '/',
            components: {
            	welcome:Welc,
            	productos:Prod,
            	shopping:Shop,
            	detall_product:DetPro,
                pedidos:Ped
            }
    }
]


const router = new VueRouter({ routes })

const app = new Vue({
    router
}).$mount('#app');