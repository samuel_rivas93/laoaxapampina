<?php

use Illuminate\Support\Facades\Route;

header('Access-Control-Allow-Origin: *');  
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
header('Access-Control-Allow-Headers: *');

Route::get('/', 'CustomController@index')->name('/');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/carta', 'CustomController@ViewCarta')->name('carta');


Route::get('/lista_productos', 'CustomController@GetProductos');
Route::get('/lista_categoria', 'CustomController@GetCategorias');
Route::get('/lista_subcategoria', 'CustomController@GetSubCategorias');
Route::post('empresa/productos', 'CustomController@PostProductos');
Route::post('guardar_categoria', 'CustomController@PostCategoria');
Route::get('editar_productos/{id_producto}', 'CustomController@EditarProducto');
Route::put('update/productos', 'CustomController@UpdateProducto');



Route::post('/agregar/carrito', 'CustomController@AgregarCarrito');
Route::get('lista_pedidos/{token_session}', 'CustomController@ListaPedidos');
Route::put('/put/cant_producto', 'CustomController@UpdateCantidadProd');
Route::delete('/pedidos/eliminar/{id_pedidos}','CustomController@EliminarPedido');
Route::post('corte', 'CustomController@TipoCorte');
Route::post('subcorte', 'CustomController@TipoSubCorte');


Route::post('/token_seguridad_niubiz', 'CustomController@Token_Seguridad');
Route::post('/token_session_niubiz', 'CustomController@Token_Session');
Route::post('/trasaction', 'CustomController@Pago_Niubiz');


Route::get('/detall_product/{id_categoria}', 'CustomController@detall_product')->name('detall_product');

Route::get('/pedidos', 'CustomController@ViewPedidos')->name('pedidos');
Route::get('/get_pedidos', 'CustomController@GetPedidos');


Route::post('/pedidos_detalles', 'CustomController@Detalles_Pedidos');

Route::get('contactanos', 'CustomController@Contacto')->name('contactanos');

Route::get('/shopping', 'CustomController@ShoppingView')->name('shopping');
Route::put('/put/cant_producto', 'CustomController@UpdateCantidadPedido');

Route::get('/clear_config', 'CustomController@ClearCache');