-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-02-2021 a las 20:03:43
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laoxapampina`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mae_categoria`
--

CREATE TABLE `mae_categoria` (
  `id_categoria` int(10) UNSIGNED NOT NULL,
  `no_categoria` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mae_categoria`
--

INSERT INTO `mae_categoria` (`id_categoria`, `no_categoria`, `created_at`, `updated_at`) VALUES
(1, 'Carnes', NULL, NULL),
(2, 'Cortes de res', NULL, NULL),
(3, 'Cortes de cerdo', NULL, NULL),
(4, 'Embutidos y Hamburguesas', NULL, NULL),
(5, 'Lacteos', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mae_productos`
--

CREATE TABLE `mae_productos` (
  `id_producto` int(10) UNSIGNED NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `no_producto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pt_producto` decimal(10,2) DEFAULT NULL,
  `qt_stock` int(11) DEFAULT NULL,
  `tx_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mae_productos`
--

INSERT INTO `mae_productos` (`id_producto`, `id_categoria`, `no_producto`, `pt_producto`, `qt_stock`, `tx_img`, `created_at`, `updated_at`) VALUES
(1, 2, 'Carne 01', '30.50', 30, 'producto-1-2c1d4c51-6dab-40be-9371-74632aff0f7f.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_23_041344_mae_categoria', 1),
(5, '2021_02_23_041841_tbl_productos', 1),
(6, '2021_02_23_041924_tbl_pedidos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_pedidos`
--

CREATE TABLE `tbl_pedidos` (
  `id_pedidos` int(10) UNSIGNED NOT NULL,
  `nu_compra` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_session` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cant_producto` int(11) DEFAULT NULL,
  `no_cliente` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tl_cliente` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_cliente` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tx_direccion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fe_compra` date DEFAULT NULL,
  `hr_compra` time DEFAULT NULL,
  `tipo_pago` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `est_pedido` int(11) DEFAULT NULL,
  `mt_total` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_pedidos`
--

INSERT INTO `tbl_pedidos` (`id_pedidos`, `nu_compra`, `token_session`, `id_producto`, `cant_producto`, `no_cliente`, `tl_cliente`, `mail_cliente`, `tx_direccion`, `fe_compra`, `hr_compra`, `tipo_pago`, `est_pedido`, `mt_total`, `created_at`, `updated_at`) VALUES
(1, NULL, 'EsJaMfkir8MtnRSUAKwj7L8shRD2ipoEJUSYriuP', 1, NULL, NULL, NULL, NULL, NULL, '2021-02-25', '10:11:57', NULL, 1, NULL, NULL, NULL),
(2, NULL, 'EsJaMfkir8MtnRSUAKwj7L8shRD2ipoEJUSYriuP', 1, NULL, NULL, NULL, NULL, NULL, '2021-02-25', '10:12:10', NULL, 1, NULL, NULL, NULL),
(3, NULL, 'RsDp8QTuaFTQna8AIUru8wZGhexShBWvpTTA1J82', 1, 4, NULL, NULL, NULL, NULL, '2021-02-25', '12:32:26', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'admin@laoxapampina.com', NULL, '$2y$10$HFb/Ig5RCgI3p.jJbO8gY.EJpmMnEJMjF.9vE1lbzRY069862KIJa', NULL, '2021-02-23 19:49:12', '2021-02-23 19:49:12');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mae_categoria`
--
ALTER TABLE `mae_categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `mae_productos`
--
ALTER TABLE `mae_productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `tbl_pedidos`
--
ALTER TABLE `tbl_pedidos`
  ADD PRIMARY KEY (`id_pedidos`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mae_categoria`
--
ALTER TABLE `mae_categoria`
  MODIFY `id_categoria` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `mae_productos`
--
ALTER TABLE `mae_productos`
  MODIFY `id_producto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tbl_pedidos`
--
ALTER TABLE `tbl_pedidos`
  MODIFY `id_pedidos` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
