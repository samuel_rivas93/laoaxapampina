<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mae_categoria extends Model
{
	protected $table='mae_categoria';
    
    protected $fillable = ['no_categoria', 'img_categoria'];

    protected $primaryKey = 'id_categoria';

    public $timestamps = false;

    protected $hidden = ['remember_token'];

}
