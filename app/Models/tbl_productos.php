<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_productos extends Model
{
	protected $table='mae_productos';

    protected $fillable = ['id_categoria', 'no_producto', 'des_producto', 'pt_producto', 'qt_stock', 'tx_img'];

    protected $primaryKey = 'id_producto';

    public $timestamps = false;

    protected $hidden = ['remember_token'];
}
