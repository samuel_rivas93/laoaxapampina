<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $no_cliente;
    public $pagar;
    public $tx_direccion;
    public $tl_cliente;
    public $numero_transaccion;
    public $listapedidos;
    public $fecha_actual;


    public function __construct($no_cliente,$tx_direccion, $tl_cliente,$numero_transaccion,
                $listapedidos, $pagar , $fecha_actual)
    {
        $this->no_cliente = $no_cliente;
        $this->pagar = $pagar;
        $this->tx_direccion = $tx_direccion;
        $this->tl_cliente = $tl_cliente;
        $this->numero_transaccion = $numero_transaccion;
        $this->listapedidos = $listapedidos;
        $this->fecha_actual = $fecha_actual;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('boleta');
    }
}
