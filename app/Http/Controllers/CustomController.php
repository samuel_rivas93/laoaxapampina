<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use App\Models\mae_categoria;
use App\Models\tbl_productos;
use App\Models\tbl_pedidos;
use App\Models\tbl_transaccion;
use App\tbl_subcategoria;
use App\Mail\OrderMail;
use Mail;

class CustomController extends Controller
{
    public function index(){

    	$token_session = Cookie::get('la_oxapampina_session');        
        $count = \DB::table('tbl_pedidos')
                ->where('tbl_pedidos.token_session', $token_session)
                ->where('tbl_pedidos.est_pedido', 1)
                ->select('tbl_pedidos.*')
                ->count();
    	return view('welcome', compact('count', 'token_session'));
    }


    public function Contacto(){
        return view('contactanos');
    }

    public function ViewPedidos(){
        return view('pedidos');
    }

    public function GetCategorias(){
    	return mae_categoria::all();
    }

    public function GetSubCategorias(){
        return tbl_subcategoria::all();
    }

    public function ClearCache()
    {
        \Artisan::call('cache:clear');
        \Artisan::call('config:clear');

        return ['message'=>'success config:cache'];
    }



    public function GetProductos(){

    	return \DB::table('mae_productos')

                ->join('mae_categoria', 'mae_productos.id_categoria','=','mae_categoria.id_categoria')            

                ->select('mae_productos.*','mae_categoria.*')

                ->get();
    }

    public function GetPedidos(){

        return tbl_pedidos::groupBy('token_session')->get();
    }

    public function PostProductos(Request $request){

        $images = $request->file('tx_img');
       
        $productos=tbl_productos::create([
            'id_categoria'=>$request['id_categoria'],
            'no_producto'=> $request['no_producto'],
            'des_producto'=> $request['des_producto'],
            'pt_producto'=> $request['pt_producto'],
            'qt_stock'=> $request['qt_stock'],
        ]);
        $cont = 0;
        foreach($images as $img){

            $custom_name = 'producto-'.$productos->id_producto.'-'.Str::uuid()->toString().'.'.$img->getClientOriginalExtension();
            if  ($cont === 0){
                $productos->tx_img = $custom_name;
            }else{
                break;
            }
            $img->move(public_path().'/productos',$custom_name);
            $productos->update();
            $cont++;
            
            
        }

        if ($productos==true) {
            return [ "status" => "success", "message" => "Producto Subido con Exito"];
        }else{
            return [ "status" => "error", "message" => "Error al Registrar sus Productos"];
        }
    }

    public function PostCategoria(Request $request){
    	mae_categoria::create([
    		'no_categoria' => $request['no_categoria']
    	]);

    	return mae_categoria::all();
    }


    public function AgregarCarrito(Request $request){
        $token_session = Cookie::get('la_oxapampina_session');
        $date=Carbon::now();
        $id_compra_cab = tbl_pedidos::create([
            'fe_compra' => $date->toDateString(),
            'hr_compra' => $request['hr_compra'],
            'token_session' => $token_session,
            'id_producto'=>$request['id_producto'],
            'cant_producto'=>$request['cant_producto'],
            'est_pedido' => 1,
        ]);

        $token_session = Cookie::get('la_oxapampina_session');

        $count =\DB::table('tbl_pedidos')
                ->where('tbl_pedidos.token_session', $token_session)
                ->where('tbl_pedidos.est_pedido', 1)
                ->select('tbl_pedidos.*')
                ->count();

        return compact('count');
    }

    public function ListaPedidos(Request $request){

        $token_session = Cookie::get('la_oxapampina_session');
        //$hr_compra = Cookie::get('hora');
        $date=Carbon::now();
        $dia=$date->toDateString();
        $hora_actual = $date->toTimeString();
        $hora_atras = $date->subHours(1)->toTimeString();
        return \DB::table('tbl_pedidos')->join('mae_productos', 'tbl_pedidos.id_producto','=','mae_productos.id_producto')                            ->whereBetween('tbl_pedidos.hr_compra', [$hora_atras,$hora_actual])
                                        ->where('tbl_pedidos.token_session', $request['token_session'])
                                        ->where('tbl_pedidos.est_pedido', 1)
                                        ->where('tbl_pedidos.fe_compra', $dia)
                                        ->select('mae_productos.*', 'tbl_pedidos.*')
                                        ->get();
    }


    public function UpdateCantidadProd(Request $request){
        $id_pedidos=$request['id_pedidos'];
        $cant_producto=$request['cant_producto'];

        $query=tbl_pedidos::where('id_pedidos', $id_pedidos)->first();
        $query->fill([
            'cant_producto'=>$cant_producto,
        ]);
        $query->save();

        if ($query==true) {
            return [ "status" => "success", "message" => "Cantidad Actualizada"];
        }else{
            return [ "status" => "error", "message" => "Error de Actualizacion"];
        }
    }


    public function TipoCorte(Request $request){
        return tbl_productos::where('id_categoria', $request['id_categoria'])->get();
    }


    public function EditarProducto(Request $request){
        return tbl_productos::where('id_producto', $request['id_producto'])->get();
    }


    public function UpdateProducto(Request $request){
        $id_producto=$request['id_producto'];

        $query=tbl_productos::where('id_producto', $id_producto)->first();
        $query->fill([
            'no_producto'=> $request['no_producto'],
            'des_producto'=> $request['des_producto'],
            'pt_producto'=> $request['pt_producto'],
            'qt_stock'=> $request['qt_stock'],
        ]);
        $query->save();

        if ($query==true) {
            return [ "status" => "success", "message" => "Producto Actualizada"];
        }else{
            return [ "status" => "error", "message" => "Error de Actualizacion"];
        }
    }

    public function EliminarPedido($id_pedidos){
        $pedido=tbl_pedidos::find($id_pedidos);
        $pedido->delete();
        return ["status"=>"success", "message"=>"Pedido Eliminado"];   
    }


    public function Token_Seguridad(){
        $response = Http::withBasicAuth(
            'laoxapampinaperu@gmail.com',
            '8zkD7!$B'
        )->post('https://apiprod.vnforapps.com/api.security/v1/security');

        return $response;
    }


    public function Token_Session(Request $request){

        $now= Carbon::now();
        $testdate = $now->toDateTimeString();

        $response = Http::withHeaders([
            'Authorization' => strval($request['token_seguridad']),
            'Content-Type'=> 'application/json'
        ])->post('https://apiprod.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/650188191', [
            "amount"=>$request['amount'],
            "antifraud"=>[
                "clientIp"=>$_SERVER['REMOTE_ADDR'],// IP del cliente
                "merchantDefineData"=>[
                    'MDD4' => $request['email'],//email del cliente
                    'MDD21' => 0,
                    'MDD32' => $request['email'],//email del cliente
                    'MDD75' => 'Invitado',
                    'MDD77' => 1,  
                ]
            ],
            "channel"=>"web"
        ]);


        return $response['sessionKey'];
    }

    public function Pago_Niubiz(Request $request){
        $total_pagar = $request['amount'];
        $email_cliente = $request['mail_cliente'];
        $tx_direccion = $request['tx_direccion'];
        $no_cliente = $request['no_cliente'];
        $tl_cliente = $request['tl_cliente'];
        $tipopago = $request['tipopago'];
        $numero_transaccion=$request['numbertransaction'];
        $hr_compra = Cookie::get('hora');


        $now= Carbon::now();
        $testdate = $now->toDateTimeString();
        $token_session = $this->Token_Seguridad();

        $response = Http::withHeaders([
            'Authorization' => strval($token_session->getBody()),
            'Content-Type'=> 'application/json'
        ])->post('https://apiprod.vnforapps.com/api.authorization/v3/authorization/ecommerce/650188191', [
            "captureType"=>"manual",
            "channel"=>"web",
            "countable"=>true,
            "order"=>[
                "amount"=>$request['amount'],
                "currency"=>"PEN",
                "tokenId"=>$request['transactionToken'],
                "purchaseNumber"=>$request['numbertransaction'],
            ]
        ]);


        if ($response->status() === 200) {
            //
            $transaccion=tbl_transaccion::create([
                'numero'=> $request['numbertransaction'] + 1,
            ]);

            $responseBody = json_decode($response->body(), true);
            //return ['status'=>'success', 'message'=>'Pago Realizado'];
            //return $responseBody;
            return $this->PaymentFinish($total_pagar, $email_cliente, $tx_direccion, $no_cliente, $tl_cliente, $tipopago,$transaccion, $hr_compra, $responseBody);
        }else{
            $transaccion=tbl_transaccion::create([
                'numero'=> $request['numbertransaction'] + 1,
            ]);
            $responseBody = json_decode($response->body(), true);
            return view('denegada',compact('email_cliente','no_cliente','tl_cliente','transaccion','responseBody','testdate'));
        }
    }

    public function PaymentFinish($total_pagar, $email_cliente, $tx_direccion, $no_cliente, $tl_cliente, $tipopago, $transaccion, $hr_compra, $responseBody){
            $token_session = Cookie::get('la_oxapampina_session');
            $asunto="Confirmación de Compra";

            $date=Carbon::now();
            $fecha_actual= $date->toDateString();
            $pagar= $total_pagar;

            $hora_actual = $date->toTimeString();
            $hora_atras = $date->subHours(1)->toTimeString();
            

            tbl_pedidos::where('token_session', $token_session)->where('nu_compra',NULL)->update([
                'nu_compra'=>$transaccion,
                'no_cliente' => $no_cliente,
                'tx_direccion' => $tx_direccion,
                'tl_cliente' => $tl_cliente,
                'est_pedido' => 0,
                'tipo_pago' => $tipopago
            ]);
            

            $listapedidos=\DB::table('tbl_pedidos')->join('mae_productos','mae_productos.id_producto', 'tbl_pedidos.id_producto')
                                        ->where('tbl_pedidos.token_session', $token_session)
                                        ->where('tbl_pedidos.fe_compra',$fecha_actual)
                                        ->whereBetween('tbl_pedidos.hr_compra', [$hora_atras,$hora_actual])
                                        ->where('tbl_pedidos.nu_compra',$transaccion)
                                        ->where('tbl_pedidos.est_pedido',0)
                                        ->select('tbl_pedidos.*', 'mae_productos.*')
                                        ->get();

         
            //return \redirect()->route('/')->with('envoit-compra','Gracias por su compra.');
            return view('boleta', compact('no_cliente','tx_direccion', 'tl_cliente','transaccion','listapedidos', 'pagar' , 'fecha_actual', 'responseBody'));

    }

    public function ShoppingView(Request $request){
        $transaccion=tbl_transaccion::select('numero')->get()->last();
        $token_session = Cookie::get('la_oxapampina_session');
        $hora = Cookie::get('hora');

        return view('shopping', compact('token_session', 'hora', 'transaccion'));
    }

    public function UpdateCantidadPedido(Request $request){
        $id_pedidos=$request['id_pedidos'];
        $cant_producto=$request['cant_producto'];

        $query=tbl_pedidos::where('id_pedidos', $id_pedidos)->first();
        $query->fill([
            'cant_producto'=>$cant_producto,
        ]);
        $query->save();

        if ($query==true) {
            $token_session = Cookie::get('la_oxapampina_session');
            $hr_compra = Cookie::get('hora');
            $date=Carbon::now();
            $dia=$date->toDateString();
            return \DB::table('tbl_pedidos')->join('mae_productos', 'tbl_pedidos.id_producto','=','mae_productos.id_producto')->where('tbl_pedidos.token_session', $token_session)->where('tbl_pedidos.est_pedido', 1)->where('tbl_pedidos.fe_compra', $dia)->orWhere('tbl_pedidos.hr_compra', $hr_compra)->select('mae_productos.*', 'tbl_pedidos.*')->get();
        }else{
            return [ "status" => "error", "message" => "Error de Actualizacion"];
        }
    }


    public function detall_product($id_categoria){
        $productos = tbl_productos::where('id_categoria', $id_categoria)->get();    
        $nombre_categoria=mae_categoria::where('id_categoria', $id_categoria)->get();

        $token_session = Cookie::get('la_oxapampina_session');        
        $count = \DB::table('tbl_pedidos')
                ->where('tbl_pedidos.token_session', $token_session)
                ->where('tbl_pedidos.est_pedido', 1)
                ->select('tbl_pedidos.*')
                ->count();
        return view('detall-product', compact('productos', 'nombre_categoria','count', 'token_session'));
    }

    public function ViewCarta(){
        $productos= \DB::table('mae_productos')

                ->join('mae_categoria', 'mae_productos.id_categoria','=','mae_categoria.id_categoria')            

                ->select('mae_productos.*','mae_categoria.*')

                ->get();
        return view('carta', compact('productos'));
    }

    public function Detalles_Pedidos(Request $request)
    {
        return \DB::table('tbl_pedidos')

                ->join('mae_productos', 'tbl_pedidos.id_producto','=','mae_productos.id_producto')  

                ->where('tbl_pedidos.token_session','=', $request['token_session'])          

                ->select('tbl_pedidos.*','mae_productos.*')

                ->get();
    }

}
